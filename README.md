_/* English version below */_

# Présentation
Ce projet rassemble un ensemble de ressources pour créer et manipuler des fichiers ODD. ODD, One Document Does it all, est très utile pour la modélisation en XML-TEI (voir https://tei-c.org/release/doc/tei-p5-doc/en/html/TD.html pour en savoir plus). Vous trouverez principalement ici, une documentation, des ODD et des feuilles de transformation XSL.

# Ressources
- **une documentation en français** sur la création d'un ODD en mode XML (sans passer par des outils comme ROMA par exemple) disponible sur le **[wiki de ce projet gitlab](https://gricad-gitlab.univ-grenoble-alpes.fr/elan/odd/-/wikis/home)**
- des feuilles de transformation XSL (XSLT) pour générer des pages HTML à partir d'un fichier ODD disponible dans ce dépôt. **Une démonstration des sorties est disponible ici : [https://elan.gricad-pages.univ-grenoble-alpes.fr/odd/](https://elan.gricad-pages.univ-grenoble-alpes.fr/odd/)**

  * `xsl/ODD2Manuel_unified.xsl` **génère un manuel HTML à partir d'un fichier ODD**
    * exemple d'entrée : `examples/ODD_Correspondances.odd`
	* exemple de sortie : `public/manuel.html` ([voir](https://elan.gricad-pages.univ-grenoble-alpes.fr/odd/manuel.html))
  * `xsl/ODD2Summary.xsl` **génère un résumé des éléments utilisés en HTML à partir d'un fichier ODD (elementSpec/@include)**
    * exemple d'entrée : `examples/ODD_Correspondances.odd`
	* exemple de sortie : `public/summary.html` ([voir](https://elan.gricad-pages.univ-grenoble-alpes.fr/odd/summary.html))
  * `xsl/TEI2Basic_ODD.xsl` **génère un ODD partiel en se basant sur les éléments et attributs utilisés dans un ensemble de fichiers XML-TEI exemple**. Ceux-ci doivent se trouver dans un sous-dossier `source/` par rapport au fichier XSL. NB. La sortie est un fichier XML-ODD bien formé mais ne doit être considéré que comme une base de travail à compléter.
    * exemple d'entrée : _à venir_
    * exemple de sortie : _à venir_
  * `xsl/TEI2HTML_editorial_page.xsl` **génère une page HTML à partir de l'encodage d'une page éditoriale**. Cette transformation gère des XML-TEI qui suivent notre schéma ODD disponible sur le dépôt git "schema-tei" (liens directs : [ODD_Editoral_pages.odd](https://gricad-gitlab.univ-grenoble-alpes.fr/elan/schemas-tei/-/blob/main/common/ODD_Editoral_pages.odd?ref_type=heads) et [ODD_Editoral_pages.rng](https://gricad-gitlab.univ-grenoble-alpes.fr/elan/schemas-tei/-/blob/main/common/out/ODD_Editoral_pages.rng?ref_type=heads))
    * exemple d'entrée : `examples/exemple_edito_corr-proust_projet.xml`
	* exemple de sortie : `public/edito_exemple_corr-proust.html` ([voir](https://elan.gricad-pages.univ-grenoble-alpes.fr/odd/edito_exemple_corr-proust.html))
	* fichier modèle pour l'encodage de pages éditoriales : `examples/modele_edito.xml` et sortie résultante : `public/edito_model.html` ([voir](https://elan.gricad-pages.univ-grenoble-alpes.fr/odd/edito_model.html))


---
# Presentation
This project contains a set of ressources to manipulate ODD files. ODD, One Document Does it all, is useful for XML-TEI modelisation (see https://tei-c.org/release/doc/tei-p5-doc/en/html/TD.html for more information). This repository contains mainly XSLT files.

# Resources
A demo of resulting outputs is available here: [https://elan.gricad-pages.univ-grenoble-alpes.fr/odd/](https://elan.gricad-pages.univ-grenoble-alpes.fr/odd/)

  * `xsl/ODD2Manuel_unified.xsl` **generate an HTML manuel based on an ODD file**
    * input example: `examples/ODD_Correspondances.odd`
	* output example: `public/manuel.html` ([see preview here](https://elan.gricad-pages.univ-grenoble-alpes.fr/odd/manuel.html))
  * `xsl/ODD2Summary.xsl` **generate an HTML summary of elements usage based on an ODD file (elementSpec/@include)**
    * input example: `examples/ODD_Correspondances.odd`
	* output example: `public/summary.html` ([see preview here](https://elan.gricad-pages.univ-grenoble-alpes.fr/odd/summary.html))
  * `xsl/TEI2Basic_ODD.xsl` **generate a partial ODD based on  elements and attributes encoded in a set of EXAMPLES example files**. Theses files must be in a subdirectory `source/` regarding to the XSL file. NB. The output is a well-formed EXAMPLES file but is nothing more than a basic work which must be completed.
    * input example: _to do_
    * output example: _to do_
  * `xsl/TEI2HTML_editorial_page.xsl` **generate an HTML page from an EXAMPLES encoding of an editorial page**. This transformation is designed for XML-TEI that follow our ODD schema available on the git repository "schema-tei" (direct links : [ODD_Editoral_pages.odd](https://gricad-gitlab.univ-grenoble-alpes.fr/elan/schemas-tei/-/blob/main/common/ODD_Editoral_pages.odd?ref_type=heads) and [ODD_Editoral_pages.rng](https://gricad-gitlab.univ-grenoble-alpes.fr/elan/schemas-tei/-/blob/main/common/out/ODD_Editoral_pages.rng?ref_type=heads))
    * input example: `examples/exemple_edito_corr-proust_projet.xml`
	* output example: `public/edito_exemple_corr-proust.html` ([see preview here](https://elan.gricad-pages.univ-grenoble-alpes.fr/odd/edito_exemple_corr-proust.html))
	* model for encoding editorial page: `examples/modele_edito.xml` and corresponding output: `public/edito_model.html` ([see preview here](https://elan.gricad-pages.univ-grenoble-alpes.fr/odd/edito_model.html))

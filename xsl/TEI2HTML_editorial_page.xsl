<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT for transformation of TEI file to HTML editorial pages
* This file was initially created for the project ENCHRE (see http://enchre.elan-numerique.fr)
* @author AnneGF@CNRS
* @date : 2022-2023
* Usage: give the html web content looking (into $xml-dir folder) for a file with //tei:text/@xml-id equal to the (final) slug of the webpage.
  * xml-dir : folder in which xml-tei files must be. Default: ../xml-tei
  * "slug" of the page (default "home") : this XSLT will look for the XPath //tei:text[@n=$slug]. If no result is found, the content will be "Page en construction".
  * [optional] "lang" (default 'false' for non-multi-lingual website): if provided, this XSLT will look for the XPath //tei:text[@n=$slug and @xml:lang=$lang] and the final slug will be $slug_$lang (e.g. "home_fr")
  * [optional] "STAND-ALONE" (default 1): if 1, generate a complete HTML page (with bootstrap inclusion) ; otherwise generate only the partial HTML content.
    If STAND-ALONE=1 and not $lang is provided, the HTML @lang will be "fr"
**/
-->
<!DOCTYPE tei2editorial [
    <!ENTITY times "&#215;">
    <!ENTITY non_breakable_space "&#160;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:sf="http://saxon.sf.net/" xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="xs tei sf" version="2.0">
    
    <xsl:param name="STAND-ALONE">1</xsl:param>
    
    <xsl:param name="xml-dir">../data/edito</xsl:param>
    <xsl:param name="slug">equipe</xsl:param>
    <xsl:param name="lang" select="false()"/>
    <!--<xsl:param name="lang">fr</xsl:param>-->
    
    
    <xsl:output method="html" indent="yes"/>
    
    <xsl:template match="/">
        <xsl:variable name="collection" select="concat($xml-dir, '?select=*.xml')"/>
        <xsl:variable name="tei_file" select="collection($collection)"/>
        <xsl:variable name="available_slugs" select="$tei_file//tei:text/@n"/>
        <xsl:for-each select="distinct-values($available_slugs)">
            <xsl:variable name="this" select="."/>
            <xsl:if test="count($available_slugs[. = $this]) > 1">
                <xsl:message>
                    <xsl:text>[Warning] More than one TEI file for the slug '</xsl:text>
                    <xsl:value-of select="$this"/>
                    <xsl:text>'. Only one will be taken into account</xsl:text>
                </xsl:message>
                <xsl:message>
                    <xsl:text>[Info] Slugs available are : </xsl:text>
                    <xsl:value-of select="$tei_file/concat(//tei:text/@n, ' (tei:title ',
                        //tei:titleStmt/tei:title, ') ')"/>
                </xsl:message>
                
            </xsl:if>
        </xsl:for-each>
        
        
        <xsl:if test="$STAND-ALONE">
            <xsl:text disable-output-escaping="yes">&lt;html lang="</xsl:text>
            <xsl:choose>
                <xsl:when test="$lang">
                    <xsl:value-of select="$lang"/>
                </xsl:when>
                <xsl:otherwise>fr</xsl:otherwise>
            </xsl:choose>
            <xsl:text disable-output-escaping="yes">"></xsl:text>
            <head>
                <!-- Required meta tags -->
                <meta charset="utf-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <!-- Bootstrap CSS -->
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
                    rel="stylesheet"
                    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
                    crossorigin="anonymous"/>
                <link href="style.css" rel="stylesheet"/>
                
                <title>
                    <xsl:value-of
                        select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"/>
                </title>
            </head>
            <xsl:text disable-output-escaping="yes">&lt;body>&lt;div class="col col-8 offset-2 mt-4 mb-4"></xsl:text>
        </xsl:if>
        
        <div class="col">
            <xsl:choose>
                <xsl:when
                    test="$lang and count($tei_file//tei:text[@n = $slug and @xml:lang = $lang]) = 1">
                    <xsl:apply-templates
                        select="$tei_file//tei:text[@n = $slug and @xml:lang = $lang]"/>
                </xsl:when>
                <xsl:when test="not($lang) and count($tei_file//tei:text[@n = $slug]) = 1">
                    <xsl:apply-templates select="$tei_file//tei:text[@n = $slug]"/>
                </xsl:when>
                <xsl:otherwise>
                    <p>
                        <i>Page en construction</i>
                    </p>
                </xsl:otherwise>
            </xsl:choose>
        </div>
        <xsl:if test="$STAND-ALONE">
            <xsl:text disable-output-escaping="yes">&lt;/div></xsl:text>
            <xsl:text disable-output-escaping="yes">&lt;/body></xsl:text>
            <xsl:text disable-output-escaping="yes">&lt;/html></xsl:text>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="tei:text">
        <xsl:choose>
            <xsl:when test="$lang">
                <xsl:apply-templates
                    select="../tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@xml:lang = $lang]"
                />
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="../tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"
                />
            </xsl:otherwise>
        </xsl:choose>
        <xsl:choose>
            <xsl:when test="name(tei:body/child::*[1]) = 'p'">
                <p class="chapeau">
                    <xsl:apply-templates select="tei:body/child::*[1]" mode="inside"/>
                </p>
                <xsl:apply-templates select="tei:body/child::*[position() > 1]"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="tei:body/child::*"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="tei:div[@synch]">
        <xsl:variable name="synch" select="@synch"/>
        <xsl:apply-templates select="/tei:TEI//tei:*[concat('#', @xml:id) = $synch]"/>
    </xsl:template>
    <xsl:template match="tei:div">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:p">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="tei:p" mode="inside">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:lb">
        <br/>
    </xsl:template>
    <xsl:template match="tei:title[parent::tei:titleStmt]">
        <h1>
            <xsl:apply-templates/>
        </h1>
    </xsl:template>
    <xsl:template match="tei:title">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>
    <xsl:template match="tei:head">
        <xsl:variable name="level" select="count(ancestor-or-self::*[ancestor::tei:body]) + 1"/>
        <xsl:element name="h{$level}">
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'italic']">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'bold']">
        <b>
            <xsl:apply-templates/>
        </b>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'underline']">
        <u>
            <xsl:apply-templates/>
        </u>
    </xsl:template>
    <xsl:template match="tei:ref">
        <a href="{@target}">
            <xsl:apply-templates/>
        </a>
    </xsl:template>
    <xsl:template match="tei:list">
        <ul>
            <xsl:apply-templates/>
        </ul>
    </xsl:template>
    <xsl:template match="tei:item">
        <li>
            <xsl:apply-templates/>
        </li>
    </xsl:template>
    <xsl:template match="tei:foreign">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>
    <xsl:template match="tei:entry">
        <div>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    <xsl:template match="tei:form">
        <b>
            <xsl:apply-templates/>
        </b>
    </xsl:template>
    <xsl:template match="tei:def">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="tei:figure">
        <img alt="{tei:figDesc}" src="{tei:graphic/@url}"/>
    </xsl:template>
    <xsl:template match="tei:email">
        <mark>
            <xsl:apply-templates/>
        </mark>
    </xsl:template>
    <xsl:template match="tei:*">
        <xsl:message>
            <xsl:text>Warning: element non pris en charge par la xsl "</xsl:text>
            <xsl:value-of select="name(.)"/>
            <xsl:text>".</xsl:text>
        </xsl:message>
        <b style="color:red">
            <xsl:value-of select="name(.)"/>
        </b>
    </xsl:template>
</xsl:stylesheet>

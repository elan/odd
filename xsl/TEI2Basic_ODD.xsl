<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT for generating ODD from one or several XML-TEI file
* version 2023-03-27
* @author AnneGF@CNRS
* @date: 2023
* modified by SerenaCC@UGA
* last edit : 22/08/2024

* Usage: Put one or more .xml files (XML-TEI) in a subfolder source/ and run a transformation on one (whatever) xml file using this XSL.
* Output: a partial ODD file containing elementSpec, and attList based on what is encoded in the XML-TEI source files
**/
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    extension-element-prefixes="tei" exclude-result-prefixes="xs tei" version="2.0">

    <xsl:output encoding="UTF-8" method="xml" indent="yes"/>


    <xsl:template match="/">
            <teiHeader>
                <fileDesc>
                    <titleStmt>
                        <title>Manuel d'encodage pour la transcription du projet *ajouter le titre*</title>
                        <respStmt>
                            <resp>Création et implémentation de l’odd à partir du *completer*</resp>
                            <name>*Prénom, Nom*</name>
                        </respStmt>
                    </titleStmt>
                    <publicationStmt>
                        <authority>UMR 5316 Litt&amp;Arts</authority>
                        <distributor sameAs="#elan">ELAN</distributor>
                        <date when="{format-date(current-date(), '[Y]-[M01]')}">
                            <xsl:value-of select="format-date(current-date(), '[MNn] [Y]', 'fr', (), ())"/>
                        </date>
                        <availability>
                            <licence target="https://spdx.org/licenses/etalab-2.0.html#licenseText">Le schéma
                                d'encodage est mis à disposition selon les termes de la licence Etalab Open License 2.0
                                (etalab-2.0)</licence>
                            <p>Ce schéma d'encodage est transformé en rng et html à l'aide d'un fichier xslt. Les
                                trois formats de sortie du schéma sont proposés selon les termes de la licence
                                Etalab-2.0.</p>
                        </availability>
                    </publicationStmt>
                    <sourceDesc>
                        <p>Généré à partir d'un fichier .xslt créé par Anne Garcia Fernandez et implementé par Serena Crespi. </p>
                    </sourceDesc>
                </fileDesc>
                <profileDesc>
                    <langUsage>
                        <language ident="fr">français</language>
                    </langUsage>
                </profileDesc>
                <revisionDesc>
                    <change></change>
                </revisionDesc>
            </teiHeader>
            <text>
                <body>
                    <schemaSpec ident="nom_du_projet" start="TEI" prefix="tei_" targetLang="fr" docLang="fr">
                        <moduleRef key="header" include="teiHeader fileDesc titleStmt publicationStmt sourceDesc"/>
                        <moduleRef key="core" include="title p"/>
                        <moduleRef key="tei"/>
                        <moduleRef key="textstructure" include="TEI text body"/>

			<xsl:variable name="xml" select="collection('source/?select=*.xml;recurse=yes')"/>
        <xsl:for-each select="$xml">
            <xsl:message>
                <xsl:text>Fichier : </xsl:text>
                <xsl:value-of select="document-uri()"/>
            </xsl:message>
        </xsl:for-each>
        <xsl:for-each-group select="$xml//*" group-by="name()">
            <!--<xsl:sort data-type="text" lang="en" select="name()" order="ascending"/>-->
            <xsl:variable name="elem" select="name()"/>
            <elementSpec ident="{name()}" mode="change" xml:id="{name()}">
                <desc type="todo"/>
                <attList>
                    <xsl:for-each-group select="current-group()/@*" group-by="name()">
                        <xsl:sort data-type="text" lang="en"/>
                        <xsl:variable name="att" select="name()"/>
                        <xsl:if
                            test="not(contains('instant default full part org status sample', name()))">
                            <attDef ident="{name()}" mode="change">
                                <desc type="todo"/>
                                <xsl:if test="
                                        not(contains('xml:id', $att))
                                        and count($xml//*[name() = $elem]/@*[name() = $att][not(starts-with(., '#'))]) > 0">
                                    <valList type="closed" mode="replace">
                                        <xsl:for-each-group
                                            select="$xml//*[name() = $elem]/@*[name() = $att]/tokenize(., ' ')"
                                            group-by=".">
                                            <xsl:sort data-type="text" lang="fr"/>
                                            <valItem ident="{.}"/>
                                        </xsl:for-each-group>
                                    </valList>
                                </xsl:if>
                            </attDef>
                        </xsl:if>
                    </xsl:for-each-group>
                </attList>
            </elementSpec>
        </xsl:for-each-group>
                    </schemaSpec>
                </body>
            </text>
      </xsl:template>
</xsl:stylesheet>

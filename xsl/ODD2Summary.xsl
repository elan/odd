<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT for generating HTML manual from ODD file
* version 2023-11-03
* @author AnneGF@CNRS
* @date : 2022-2023
* last edit : 03/11/2023
**/
-->
<!DOCTYPE mon-document [
  <!ENTITY times "&#215;">
  <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs tei sch" version="2.0"
    xmlns:sch="http://purl.oclc.org/dsdl/schematron" xmlns:tei="http://www.tei-c.org/ns/1.0">

    <xsl:output method="html" omit-xml-declaration="yes" indent="yes"/>
    <xsl:strip-space elements="*"/>
    <xsl:variable name="STAND-ALONE">1</xsl:variable>
    <!--<xsl:preserve-space elements=""/>-->

    <xsl:template match="/tei:TEI">
        <xsl:if test="$STAND-ALONE">
            <xsl:text disable-output-escaping="yes">&lt;html lang="fr"></xsl:text>
            <head>
                <!-- Required meta tags -->
                <meta charset="utf-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <!-- Bootstrap CSS -->
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
                    rel="stylesheet"
                    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
                    crossorigin="anonymous"/>
                <link href="style.css" rel="stylesheet"/>

                <title>Sommaire généré depuis ODD - <xsl:value-of
                        select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"/></title>
            </head>
            <xsl:text disable-output-escaping="yes">&lt;body></xsl:text>
        </xsl:if>
        <div class="col-10 offset-1">
            <h1 class="fs-2 text-center">
                <xsl:value-of select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"/>
                <br/>
                <span class="fs-3">
                    <xsl:text>Résumé des éléments utilisés</xsl:text>
                </span>
            </h1>
            <table class="table table-bordered table-sm">
                <thead class="text-center">
                    <th scope="colgroup">
                        <xsl:attribute name="colspan">
                            <xsl:value-of
                                select="count(distinct-values(//tei:schemaSpec/tei:moduleRef/@key))"
                            />
                        </xsl:attribute>
                        <xsl:text>Éléments TEI classés par module</xsl:text>
                    </th>
                </thead>
                <thead>
                    <xsl:for-each select="//tei:schemaSpec/tei:moduleRef[not(@key = 'tei')]">
                        <xsl:sort data-type="text" xml:lang="fr" order="ascending" select="@key"/>
                        <th scope="col">
                            <xsl:value-of select="@key"/>
                        </th>
                    </xsl:for-each>
                </thead>
                <xsl:variable name="max"
                    select="max(//tei:schemaSpec/tei:moduleRef[not(@key = 'tei')]/count(tokenize(@include, ' ')))"/>
                <xsl:variable name="bigger"
                    select="//tei:schemaSpec/tei:moduleRef[not(@key = 'tei')][count(tokenize(@include, ' ')) = $max]/@key"/>
                <xsl:variable name="schemaSpec" select="//tei:schemaSpec"/>
                <xsl:for-each select="//tei:schemaSpec/tei:moduleRef[not(@key = 'tei')][@key = $bigger]/tokenize(@include)">
                    <tr class="fs-6">
                        <xsl:variable name="position" select="position()"/>
                        <xsl:for-each select="$schemaSpec/tei:moduleRef[not(@key = 'tei')]">
                            <xsl:sort data-type="text" xml:lang="fr" order="ascending" select="@key"/>
                            <xsl:for-each select="tokenize(@include, ' ')">
                                <xsl:sort data-type="text" xml:lang="fr" order="ascending" select="."/>
                                <xsl:if test="position() = $position">
                                    <td>
                                        <code>
                                            <xsl:variable name="elementName" select="."/>
                                            <a>
                                                <xsl:attribute name="href">
                                                    <xsl:text>https://tei-c.org/release/doc/tei-p5-doc/fr/html/ref-</xsl:text>
                                                    <xsl:value-of select="$elementName"/>
                                                    <xsl:text>.html</xsl:text>
                                                </xsl:attribute>
                                                <xsl:value-of select="$elementName"/>
                                            </a>
                                        </code>
                                    </td>
                                </xsl:if>
                            </xsl:for-each>
                            <xsl:if test="count(tokenize(@include, ' ')) &lt; $position">
                                <td/>
                            </xsl:if>
                        </xsl:for-each>
                    </tr>
                </xsl:for-each>
                
                <tfoot>
                    <xsl:for-each select="//tei:schemaSpec/tei:moduleRef[not(@key = 'tei')]">
                        <xsl:sort data-type="text" xml:lang="fr" order="ascending" select="@key"/>
                        <th scope="col">
                            <xsl:value-of select="count(tokenize(@include, ' '))"/>
                        </th>
                    </xsl:for-each>
                </tfoot>
            </table>
        </div>
        <xsl:if test="$STAND-ALONE">
            <xsl:text disable-output-escaping="yes">&lt;/body></xsl:text>
            <xsl:text disable-output-escaping="yes">&lt;/html></xsl:text>
        </xsl:if>
    </xsl:template>


    <!-- DEBUG SECTION - Templates used to show warnings if unexpected elements or attributes -->
    <xsl:template match="tei:*">
        <span class="btn-warning btn-sm" title="Élément non pris en charge par la XSLT.">
            <xsl:text>Élément "</xsl:text>
            <xsl:value-of select="name(.)"/>
            <xsl:text>" non pris en charge par la transformation XSL.</xsl:text>
        </span>
    </xsl:template>
    <!-- END DEBUG SECTION -->
</xsl:stylesheet>

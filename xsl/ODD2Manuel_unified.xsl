<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT for generating HTML manual from ODD file
* creation date 2023-03-27
* @author AnneGF@CNRS
* @modified by @SerenaCC (2023-2024) : first draft for table of content
* @modified by FannyM (2024) : add TEI documentation - Attention : ne prend pas en compte les modes
* @modified by AnneGF@CNRS (2024) : add param for TOC and allowing two main manual organisation (by module, 
                     based on <schemaSpec/> declarations or reading a manually written doc in body/text/div)
**/
-->
<!DOCTYPE mon-document [
  <!ENTITY times "&#215;">
  <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs tei sch" version="2.0"
    xmlns:sch="http://purl.oclc.org/dsdl/schematron" xmlns:html="http://www.w3.org/1999/xhtml"
    xmlns:tei="http://www.tei-c.org/ns/1.0">

    <xsl:output method="html" omit-xml-declaration="yes" indent="yes"/>
    <xsl:strip-space elements="*"/>
    <xsl:param name="STAND-ALONE" as="xs:boolean">1</xsl:param>
    <!--<xsl:preserve-space elements=""/>-->

    <!-- Mettre le lien vers un dossier avec la documentation de la TEI, trouvable ici : https://sourceforge.net/projects/tei/ -->
    <xsl:param name="documentation_TEI">../tei-4.8.1/doc/tei-p5-doc/fr/html/ref-</xsl:param>

    <xsl:param name="with_toc" select="true()" as="xs:boolean"/>
    <!-- false() if the table of content should not be generated -->
    <xsl:param name="toc_by_module" select="false()" as="xs:boolean"/>
    <!-- if false(), this xsl will look for @xml:id and create a TOC based on that
         if true(), TOC based on module (only if explore_schemaSpec is true(), otherwise no TOC
    -->

    <xsl:param name="explore_schemaSpec" select="true()" as="xs:boolean"/>
    <!-- if false(), we assume a manual is written somewhere in TEO/text/body/div and references
        to element that need to be presented in the manual are called using <pecGrpRef/> -->

    <!-- Keep the ODD in a variable -->
    <xsl:variable name="odd" select="."/>

    <xsl:template match="/tei:TEI">
        <xsl:if test="$STAND-ALONE">
            <xsl:text disable-output-escaping="yes">&lt;html lang="fr"></xsl:text>
            <head>
                <!-- Required meta tags -->
                <meta charset="utf-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <!-- Bootstrap CSS -->
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
                    rel="stylesheet"
                    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
                    crossorigin="anonymous"/>
                <link href="style.css" rel="stylesheet"/>

                <title>Manuel généré depuis ODD - <xsl:value-of
                        select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"/></title>
            </head>
            <xsl:text disable-output-escaping="yes">&lt;body></xsl:text>
        </xsl:if>

        <!-- Check completeness of the ODD generating html alert message at the top of the page-->
        <xsl:for-each select="//tei:elementSpec/@ident | //tei:elementRef/@key">
            <xsl:variable name="elem">
                <xsl:text> </xsl:text>
                <xsl:value-of select="."/>
                <xsl:text> </xsl:text>
            </xsl:variable>
            <xsl:variable name="list">
                <xsl:text> </xsl:text>
                <xsl:for-each select="//tei:moduleRef/@include/tokenize(., ' ')">
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="."/>
                    <xsl:text> </xsl:text>
                </xsl:for-each>
                <xsl:text> </xsl:text>
            </xsl:variable>
            <xsl:if test="not(matches($list, $elem))">
                <div class="btn-danger btn-sm">
                    <xsl:text>L'élément </xsl:text>
                    <code class="fw-bold text-primary">
                        <xsl:value-of select="$elem"/>
                    </code>
                    <xsl:text> est utilisé sans être inclu dans aucun module. Veuillez l'ajouter dans l'attribut </xsl:text>
                    <code class="fw-bold text-primary">@include</code>
                    <xsl:text> du module auquel il appartient (cf. </xsl:text>
                    <a target="_blank">
                        <xsl:attribute name="href">
                            <xsl:text>https://tei-c.org/release/doc/tei-p5-doc/fr/html/ref-</xsl:text>
                            <xsl:value-of select="$elem"/>
                            <xsl:text>.html</xsl:text>
                        </xsl:attribute>
                        <xsl:text>doc de la TEI</xsl:text>
                    </a>
                    <xsl:text>).</xsl:text>
                </div>
            </xsl:if>
        </xsl:for-each>

        <!-- Begin manual -->
        <div class="col-10 offset-1">
            <h1 class="display-4 fs-2 text-center">
                <xsl:value-of select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"/>
            </h1>
            <xsl:if test="$with_toc">
                <xsl:message>Generating TOC</xsl:message>
                <xsl:choose>
                    <xsl:when test="$toc_by_module">
                        <xsl:message>&#160; by module</xsl:message>
                        <xsl:choose>
                            <xsl:when test="$explore_schemaSpec">
                                <div class="TOC">
                                    <h2>Table de matières</h2>
                                    <ul>
                                        <xsl:for-each select=".//tei:moduleRef">
                                            <li>
                                                <a href="#{@key}">
                                                  <xsl:text>Module </xsl:text>
                                                  <xsl:value-of select="@key"/>
                                                </a>
                                            </li>
                                        </xsl:for-each>
                                    </ul>
                                </div>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:message>CANCELED (not TOC by module if explore_schemaSpec is
                                    false()</xsl:message>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:choose>
                            <xsl:when test="exists(.//tei:head[@xml:id])">
                                <xsl:message>&#160; by declared &lt;head/> with
                                    @xml:id</xsl:message>
                                <div class="TOC">
                                    <h2>Table de matières</h2>
                                    <ul>
                                        <xsl:for-each select=".//tei:head">
                                            <!--
                                                Change level depending on position of the head in the hierarchy
                                                TODO: Make it cleaner... Without using xsl:text disabled output and li style="list-style: none;"..
                                            -->
                                            <!-- Useful for debug only <xsl:message>
                                                <xsl:value-of select="count(ancestor::*)"/>
                                                <xsl:text> ? </xsl:text>
                                                <xsl:value-of select="preceding::tei:head[1]/count(ancestor::*)"/>
                                            </xsl:message>-->
                                            <xsl:choose>
                                                <xsl:when
                                                  test="count(ancestor::*) > preceding::tei:head[1]/count(ancestor::*)">
                                                  <xsl:text disable-output-escaping="yes">&lt;li style="list-style: none;">&lt;ul style="padding-top: 0;"></xsl:text>
                                                  <li>
                                                  <a href="#{@xml:id}">
                                                  <xsl:value-of select="."/>
                                                  </a>
                                                  </li>
                                                </xsl:when>
                                                <xsl:when
                                                  test="count(ancestor::*) = preceding::tei:head[1]/count(ancestor::*)">
                                                  <li>
                                                  <a href="#{@xml:id}">
                                                  <xsl:value-of select="."/>
                                                  </a>
                                                  </li>
                                                </xsl:when>
                                                <xsl:when
                                                  test="count(ancestor::*) &lt; preceding::tei:head[1]/count(ancestor::*)">
                                                  <xsl:text disable-output-escaping="yes">&lt;/ul>&lt;/li></xsl:text>
                                                  <li>
                                                  <a href="#{@xml:id}">
                                                  <xsl:value-of select="."/>
                                                  </a>
                                                  </li>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <li>
                                                        <a href="#{@xml:id}">
                                                            <xsl:value-of select="."/>
                                                        </a>
                                                    </li>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:for-each>
                                    </ul>
                                </div>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:message>
                                    <xsl:text>[Warning] No &lt;tei:head/> with @xml:id declared. No TOC generated.</xsl:text>
                                </xsl:message>
                                <xsl:message/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:message/>
            </xsl:if>
            <xsl:choose>
                <xsl:when test="$explore_schemaSpec">
                    <xsl:message>Explore schemaSpec</xsl:message>
                    <xsl:apply-templates select="tei:text/tei:body//tei:schemaSpec"
                        mode="explore_schemaSpec"/>
                    <xsl:message>&#160; DONE</xsl:message>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:message>Do not explore schemaSpec</xsl:message>
                    <xsl:apply-templates select="tei:text/tei:body/*"/>
                    <xsl:message>&#160; DONE</xsl:message>
                </xsl:otherwise>
            </xsl:choose>
            <hr/>
            <h3>Remarques générales sur le modèle TEI et son manuel</h3>
            <ul>
                <li>
                    <xsl:text>Modules et éléments utilisés&nbsp;:</xsl:text>
                    <ul>
                        <xsl:apply-templates select="//tei:moduleRef" mode="information"/>
                    </ul>
                </li>
                <xsl:if test="count(//tei:classRef) > 0">
                    <li>
                        <xsl:text>Classes utilisées&nbsp;:</xsl:text>
                        <ul>
                            <xsl:apply-templates select="//tei:classRef" mode="information"/>
                        </ul>
                    </li>
                </xsl:if>
                <xsl:if test="//tei:classSpec[not(@mode = 'delete')]">
                    <li>
                        <xsl:text>Classes modifiées&nbsp;:</xsl:text>
                        <ul>
                            <xsl:apply-templates select="//tei:classSpec[not(@mode = 'delete')]"
                                mode="information"/>
                        </ul>
                    </li>
                </xsl:if>
                <xsl:if test="//tei:classSpec[@mode = 'delete']">
                    <li>
                        <xsl:text>Classes supprimées&nbsp;:</xsl:text>
                        <ul>
                            <xsl:for-each select="//tei:classSpec[@mode = 'delete']">
                                <xsl:apply-templates select="." mode="information"/>
                                <xsl:if test="position() != last()">
                                    <xsl:text>, </xsl:text>
                                </xsl:if>
                            </xsl:for-each>
                        </ul>
                    </li>
                </xsl:if>
                <xsl:for-each-group select="//tei:elementSpec/@ident | tei:elementRef/@key"
                    group-by=".">
                    <xsl:if test="count(current-group()) > 1">
                        <li class="warning btn-warning btn-sm">
                            <xsl:text>L'élément </xsl:text>
                            <code>
                                <xsl:value-of select="current-grouping-key()"/>
                            </code>
                            <xsl:text> est spécifié plusieurs fois.</xsl:text>
                        </li>
                    </xsl:if>
                </xsl:for-each-group>

                <xsl:for-each select="//tei:specGrpINVALIDE-TEMPORAIREMENT-VOIR-AVEC-FM-ET-AG">
                    <!--<h3 id="{@key}"><xsl:value-of select="@key"/></h3>-->
                    <h3 id="{@xml:id}">
                        <xsl:value-of select="@xml:id"/>
                    </h3>
                    <xsl:for-each select="tokenize(@include, ' ')">

                        <xsl:if
                            test="not($odd//tei:elementSpec[@ident = current()] and $odd//tei:elementRef[@key = current()])">

                            <!-- @FM :  ajout de la doc TEI pour les éléments uniquement présents dans le include (remplace le btn-warning) / TODO : gérer l'ordre au lieu de tout mettre à la fin -->

                            <h4><xsl:attribute name="id">
                                    <xsl:value-of select="."/>
                                </xsl:attribute> Élément <code><xsl:value-of select="."
                                /></code></h4>

                            <p>
                                <!-- met un attribut tei_doc dans le nouveau desc -->
                                <xsl:attribute name="id">
                                    <xsl:text>tei_doc</xsl:text>
                                </xsl:attribute>
                                <xsl:variable name="html_name"
                                    select="concat($documentation_TEI, ., '.html')"/>
                                <xsl:variable name="extrait_tei" select="document($html_name)"/>
                                <xsl:value-of
                                    select="substring-after(substring-before($extrait_tei//html:div[@class = 'main-content']/html:div/html:table/html:tr[1]/html:td[1], '['), '&gt;')"/>

                            </p>

                        </xsl:if>
                    </xsl:for-each>
                </xsl:for-each>
            </ul>
        </div>
        <xsl:if test="$STAND-ALONE">
            <xsl:text disable-output-escaping="yes">&lt;/body></xsl:text>
            <xsl:text disable-output-escaping="yes">&lt;/html></xsl:text>
        </xsl:if>

    </xsl:template>

    <xsl:template match="tei:schemaSpec"/>
    <xsl:template match="tei:schemaSpec" mode="explore_schemaSpec">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template match="tei:moduleRef"/>
    <xsl:template match="tei:moduleRef" mode="explore_schemaSpec">
        <h3 id="{@key}">
            <xsl:text>Module </xsl:text>
            <xsl:value-of select="@key"/>
        </h3>
        <xsl:choose>
            <xsl:when test="@include and not(@include = '')">
                <xsl:for-each select="tokenize(@include, ' ')">
                    <xsl:variable name="elem" select="."/>
                    <xsl:apply-templates select="$odd//tei:elementSpec[@ident = $elem]"/>
                </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
                <p>Ce module est inclut dans son intégralité. Voir la documentation officielle de la
                    TEI pour plus d'information.</p>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="tei:moduleRef" mode="information" name="module_info">
        <li>
            <xsl:text>Module </xsl:text>
            <i>
                <xsl:value-of select="@key"/>
            </i>
            <xsl:if test="@include">
                <xsl:text> (élément</xsl:text>
                <xsl:if test="contains(normalize-space(@include), ' ')">
                    <xsl:text>s</xsl:text>
                </xsl:if>
                <xsl:text> </xsl:text>
                <xsl:for-each select="tokenize(normalize-space(@include), ' ')">
                    <code>
                        <xsl:value-of select="."/>
                    </code>
                    <xsl:choose>
                        <xsl:when test="position() lt last() - 1">
                            <xsl:text>, </xsl:text>
                        </xsl:when>
                        <xsl:when test="position() eq (last() - 1)">
                            <xsl:text> et </xsl:text>
                        </xsl:when>
                        <xsl:otherwise/>
                    </xsl:choose>
                </xsl:for-each>
                <xsl:text>)</xsl:text>
            </xsl:if>
            <xsl:if test="@exclude">
                <xsl:text> (sauf </xsl:text>
                <xsl:choose>
                    <xsl:when test="contains(@include, ' ')">
                        <xsl:text>les éléments</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>l'élément</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:text> </xsl:text>
                <xsl:for-each select="tokenize(@include, ' ')">
                    <code>
                        <xsl:value-of select="."/>
                    </code>
                    <xsl:choose>
                        <xsl:when test="position() lt last() - 1">
                            <xsl:text>, </xsl:text>
                        </xsl:when>
                        <xsl:when test="position() eq (last() - 1)">
                            <xsl:text> et </xsl:text>
                        </xsl:when>
                        <xsl:otherwise/>
                    </xsl:choose>
                </xsl:for-each>
                <xsl:text>)</xsl:text>
            </xsl:if>
        </li>
    </xsl:template>
    <xsl:template match="tei:specGrp"/>
    <xsl:template match="tei:specGrpRef">
        <div>
            <xsl:variable name="id" select="substring-after(@target, '#')"/>
            <xsl:attribute name="href">
                <xsl:value-of select="./@target"/>
            </xsl:attribute>
            <xsl:apply-templates select="//tei:specGrp[@xml:id = $id]/*"/>
        </div>
    </xsl:template>

    <xsl:template match="tei:classRef"/>
    <xsl:template match="tei:classRef" mode="information">
        <li style="display: inline list-item;">
            <a>
                <xsl:attribute name="href">
                    <xsl:text>https://www.tei-c.org/Vault/P5/2.0.2/doc/tei-p5-doc/en/html/ref-</xsl:text>
                    <xsl:value-of select="@key"/>
                    <xsl:text>.html</xsl:text>
                </xsl:attribute>
                <xsl:value-of select="@key"/>
            </a>
        </li>
    </xsl:template>
    <xsl:template match="tei:classSpec"/>
    <xsl:template match="tei:classSpec[not(@mode = 'delete')]" mode="information">
        <li>
            <a>
                <xsl:attribute name="href">
                    <xsl:text>https://www.tei-c.org/Vault/P5/2.0.2/doc/tei-p5-doc/en/html/ref-</xsl:text>
                    <xsl:value-of select="@ident"/>
                    <xsl:text>.html</xsl:text>
                </xsl:attribute>
                <xsl:value-of select="@ident"/>
            </a>
            <xsl:text>&nbsp;: </xsl:text>
            <xsl:for-each-group select="tei:attList/tei:attDef" group-by="@mode">
                <xsl:choose>
                    <xsl:when test="@mode = 'delete'">attributs supprimés&nbsp;(</xsl:when>
                    <xsl:otherwise>@mode '<xsl:value-of select="@mode"/>'&nbsp;(</xsl:otherwise>
                </xsl:choose>
                <xsl:for-each select="current-group()">
                    <code>
                        <xsl:text>@</xsl:text>
                        <xsl:value-of select="@ident"/>
                    </code>
                    <xsl:if test="not(position() = last())">, </xsl:if>
                </xsl:for-each>
                <xsl:text>)</xsl:text>
                <xsl:if test="not(position() = last())">, </xsl:if>
            </xsl:for-each-group>
        </li>
    </xsl:template>
    <xsl:template match="tei:classSpec[@mode = 'delete']" mode="information">
        <a>
            <xsl:attribute name="href">
                <xsl:text>https://www.tei-c.org/Vault/P5/2.0.2/doc/tei-p5-doc/en/html/ref-</xsl:text>
                <xsl:value-of select="@ident"/>
                <xsl:text>.html</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="@ident"/>
        </a>
    </xsl:template>

    <xsl:template match="tei:elementSpec | tei:elemenrRef">
        <h4><xsl:attribute name="id">
                <xsl:value-of select="./@xml:id"/>
            </xsl:attribute> Élément <code><xsl:value-of select="@ident"/></code></h4>
        <xsl:if
            test="not(tei:desc) and not(tei:exemplum) and not(tei:remarks) and not(tei:constraintSpec)">
            <xsl:message select="name()"/>
            <!-- @FM ajout de la doc TEI s'il n'y a pas de description -->
            <p>
                <!-- met un attribut tei_doc dans le nouveau desc -->
                <xsl:attribute name="id">
                    <xsl:text>tei_doc</xsl:text>
                </xsl:attribute>
                <xsl:variable name="html_name" select="concat($documentation_TEI, @xml:id, '.html')"/>
                <xsl:variable name="extrait_tei" select="document($html_name)"/>
                <xsl:value-of
                    select="substring-after(substring-before($extrait_tei//html:div[@class = 'main-content']/html:div/html:table/html:tr[1]/html:td[1], '['), '&gt;')"
                />
            </p>
        </xsl:if>
        <xsl:apply-templates/>
        <xsl:if test="tei:constraintSpec">
            <xsl:text>Contrainte</xsl:text>
            <xsl:if test="count(tei:constraintSpec) > 1">
                <xsl:text>s</xsl:text>
            </xsl:if>
            <xsl:text>&nbsp;:</xsl:text>
            <ul>
                <xsl:apply-templates select="tei:constraintSpec" mode="show-constraints"/>
            </ul>
        </xsl:if>
    </xsl:template>

    <xsl:template match="tei:constraintSpec"/>
    <xsl:template match="tei:constraintSpec" mode="show-constraints">
        <li>
            <xsl:apply-templates/>
        </li>
    </xsl:template>
    <xsl:template match="tei:constraint">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="sch:name">
        <code>
            <xsl:value-of select="ancestor::tei:elementSpec/@ident | ancestor::tei:element/@key"/>
        </code>
    </xsl:template>


    <xsl:template match="tei:exemplum">
        <xsl:text>Exemple d'utilisation&nbsp;:</xsl:text>
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:attList">
        <xsl:if test="count(tei:attDef[not(@mode = 'delete')]) > 0">
            <xsl:text>Attribut</xsl:text>
            <xsl:if test="count(tei:attDef[not(@mode = 'delete')]) > 1">
                <xsl:text>s</xsl:text>
            </xsl:if>
            <xsl:text>&nbsp;:</xsl:text>
            <ul>
                <xsl:apply-templates/>
            </ul>
        </xsl:if>
        <!-- TODO : complete with others @mode -->
    </xsl:template>
    <xsl:template match="tei:attDef">
        <xsl:if test="not(@mode = 'delete')">
            <li>
                <code>@<xsl:value-of select="@ident"/></code>&nbsp; <i><xsl:apply-templates
                        select="tei:desc" mode="inline"/></i>
                <xsl:if test="tei:datatype">
                    <xsl:apply-templates select="tei:datatype"/>
                </xsl:if>
                <xsl:if test="tei:valList">
                    <xsl:apply-templates select="tei:valList"/>
                </xsl:if>
                <xsl:apply-templates
                    select="child::*[not(name(.) = 'valList') and not(name(.) = 'desc') and not(name(.) = 'datatype')]"
                />
            </li>
        </xsl:if>
    </xsl:template>
    <xsl:template match="tei:datatype">
        <!--<xsl:text> (de type&nbsp;: </xsl:text>
        <a>
            <xsl:attribute name="href">
                <xsl:text>https://www.tei-c.org/release/doc/tei-p5-doc/fr/html/ref-</xsl:text>
                <xsl:value-of select="tei:dataRef/@key"/>
                <xsl:text>.html</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="tei:dataRef/@key"/>
        </a>-->
        <xsl:if test="@minOccurs or @maxOccurs">
            <!-- <xsl:text> - </xsl:text> -->
            <xsl:text> (</xsl:text>
            <xsl:choose>
                <xsl:when test="@minOccurs > 0 and (not(@maxOccurs) or @maxOccurs = 'unbounded')">
                    <xsl:text>au moins </xsl:text>
                    <xsl:value-of select="@minOccurs"/>
                    <xsl:text> valeur</xsl:text>
                    <xsl:if test="@minOccurs > 1"> </xsl:if>
                </xsl:when>
                <xsl:when test="@minOccurs > 0">
                    <xsl:value-of select="@minOccurs"/>
                    <xsl:text> à </xsl:text>
                    <xsl:value-of select="@maxOccurs"/>
                    <xsl:text> valeur</xsl:text>
                    <xsl:if test="@maxOccurs > 2">
                        <xsl:text>s</xsl:text>
                    </xsl:if>
                </xsl:when>
                <xsl:when test="not(@maxOccurs = 'unbounded')">
                    <xsl:text>au plus </xsl:text>
                    <xsl:value-of select="@maxOccurs"/>
                    <xsl:text> valeur</xsl:text>
                    <xsl:if test="@maxOccurs > 1"> </xsl:if>
                    <xsl:text> possible</xsl:text>
                    <xsl:if test="@maxOccurs > 2">
                        <xsl:text>possibles</xsl:text>
                    </xsl:if>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>plusieurs valeurs possibles</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>
    <xsl:template match="tei:desc" mode="inline">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:desc" mode="inline-parenthesis">
        <xsl:value-of
            select="replace(concat(lower-case(substring(., 1, 1)), substring(., 2)), '.$', '')"/>
    </xsl:template>
    <xsl:template match="tei:desc">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="tei:att">
        <code>
            <xsl:text>@</xsl:text>
            <xsl:apply-templates/>
        </code>
    </xsl:template>
    <xsl:template match="tei:valList">
        <!-- @AGF : pour supprimer des listes dans le manuel mais les laisser dans le .rng :
            <xsl:choose>
             <xsl:when test="@style = 'display:none'">
                 <xsl:apply-templates/>                
             </xsl:when>
             <xsl:otherwise>
                 <ul>
                 <xsl:choose>
                     <xsl:when test="count(tei:valItem) &lt;= 5">
                         <xsl:for-each select="tei:valItem">
                             <xsl:sort select="@ident" lang="fr"/>
                             <xsl:apply-templates select="."/>
                         </xsl:for-each>
                     </xsl:when>
                     <xsl:otherwise>
                         <xsl:for-each select="tei:valItem">
                             <xsl:sort select="@ident" lang="fr"/>
                             <xsl:apply-templates select="." mode="inline"/>
                             <xsl:if test="position() &lt; last()">
                                 <xsl:text>, </xsl:text>
                             </xsl:if>
                         </xsl:for-each>
                     </xsl:otherwise>
                 </xsl:choose>
                </ul>
             </xsl:otherwise>
            </xsl:choose>? -->
        <ul>
            <xsl:choose>
                <xsl:when test="count(tei:valItem)">
                    <!--  EXPRESSION BEFORE MODIFICATIONS : <xsl:when test="count(tei:valItem) &lt;=5">-->
                    <xsl:for-each select="tei:valItem">
                        <xsl:sort select="@ident" lang="fr"/>
                        <xsl:apply-templates select="."/>
                    </xsl:for-each>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:for-each select="tei:valItem">
                        <xsl:sort select="@ident" lang="fr"/>
                        <xsl:apply-templates select="." mode="inline"/>
                        <xsl:if test="position() &lt; last()">
                            <xsl:text>, </xsl:text>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:otherwise>
            </xsl:choose>
        </ul>
    </xsl:template>
    <xsl:template match="tei:valItem">
        <li>
            <code>
                <xsl:value-of select="@ident"/>
            </code>
            <xsl:if test="not(. = '')">&nbsp;: <xsl:apply-templates mode="inline"/></xsl:if>
        </li>
    </xsl:template>
    <xsl:template match="tei:valItem" mode="inline">
        <code>
            <xsl:value-of select="@ident"/>
        </code>
        <xsl:if test="not(. = '')">&nbsp;<small>(<xsl:apply-templates mode="inline-parenthesis"
                />)</small></xsl:if>
    </xsl:template>

    <xsl:template match="tei:eg">
        <pre>
            <div style="border: 1px solid #696969; padding: 5px;">
    <xsl:apply-templates/>
  </div></pre>
    </xsl:template>
    <xsl:template match="tei:remarks">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:emph">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>
    <xsl:template match="tei:p">
        <xsl:choose>
            <xsl:when test="@style = 'example'">
                <xsl:text>Exemple d'utilisation&nbsp;:</xsl:text>
                <p style="display: block; 
                    font-size: medium; 
                    font-style: italic; 
                    margin-left: 40px;
                    margin-right: 600px;">
                    <xsl:apply-templates/>
                </p>
            </xsl:when>
            <xsl:otherwise>
                <p>
                    <xsl:apply-templates/>
                </p>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="tei:hi">
        <xsl:choose>
            <xsl:when test="@rend = 'italic'">
                <i>
                    <xsl:apply-templates/>
                </i>
            </xsl:when>
            <xsl:when test="@rend = 'bold'">
                <b>
                    <xsl:apply-templates/>
                </b>
            </xsl:when>
            <xsl:when test="@rend = 'strong'">
                <strong>
                    <xsl:apply-templates/>
                </strong>
            </xsl:when>
            <xsl:when test="@rend = 'underline'">
                <u>
                    <xsl:apply-templates/>
                </u>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="tei:content"/>
    <xsl:template match="tei:ref">
        <a>
            <xsl:attribute name="href">
                <xsl:value-of select="./@target"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </a>
    </xsl:template>
    <xsl:template match="tei:lb">
        <br>
            <xsl:apply-templates/>
        </br>
    </xsl:template>
    <xsl:template match="tei:div">
        <div>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    <xsl:template match="tei:title">
        <mark>
            <xsl:apply-templates/>
        </mark>
    </xsl:template>
    <xsl:template match="tei:floatingText">
        <bloc>
            <xsl:apply-templates/>
        </bloc>
    </xsl:template>
    <xsl:template match="tei:floatingText/tei:body">
        <bloc>
            <xsl:apply-templates/>
        </bloc>
    </xsl:template>
    <xsl:template match="tei:figure">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:code">
        <b>
            <pre>
    <xsl:apply-templates/>
        </pre>
        </b>
    </xsl:template>
    <xsl:template match="tei:figDesc">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="tei:graphic">
        <br/>
        <img>
            <xsl:attribute name="alt">
                <xsl:value-of select="'image'"/>
            </xsl:attribute>
            <xsl:attribute name="src">
                <xsl:value-of select="./@url"/>
            </xsl:attribute>
            <xsl:attribute name="style">
                <xsl:choose>
                    <xsl:when test="contains(@rend, 'inline')">
                        <xsl:text>display: inline;</xsl:text>
                    </xsl:when>
                    <xsl:when test="preceding-sibling::node()[1][self::text()]">
                        <xsl:text>display: inline-block;</xsl:text>
                    </xsl:when>
                </xsl:choose>
                <xsl:if test="contains(@rend, 'left')">
                    <xsl:text>float: left; margin-right: 15px;</xsl:text>
                </xsl:if>
                <xsl:if test="contains(@rend, 'right')">
                    <xsl:text>float: right; margin-left: 15px;</xsl:text>
                </xsl:if>
            </xsl:attribute>
            <xsl:choose>
                <xsl:when test="@scale and matches(@scale, '[0-9][0-9]?')">
                    <xsl:attribute name="width">
                        <xsl:value-of select="@scale"/>
                        <xsl:text>%</xsl:text>
                    </xsl:attribute>
                </xsl:when>
            </xsl:choose>
        </img>
    </xsl:template>
    <xsl:template match="tei:anchor">
        <a>
            <xsl:attribute name="href">
                <xsl:value-of select="concat('#', ./@xml:id)"/>
            </xsl:attribute>
            <xsl:text>Haut de page</xsl:text>
        </a>
    </xsl:template>
    <xsl:template match="tei:head">
        <!-- Niveau du titre donné selon la "distance" à l'élément <body/> -->
        <xsl:variable name="level" select="count(ancestor-or-self::*[ancestor::tei:body]) + 1"/>
        <xsl:element name="h{$level}">
            <xsl:if test="not(@xml:id = '')">
                <xsl:attribute name="id">
                    <xsl:value-of select="@xml:id"/>
                </xsl:attribute>
            </xsl:if>
            <b>
                <xsl:apply-templates/>
            </b>
        </xsl:element>
    </xsl:template>
    <xsl:template match="tei:list">
        <ul>
            <xsl:apply-templates/>
        </ul>
    </xsl:template>
    <xsl:template match="tei:item">
        <li>
            <xsl:apply-templates/>
        </li>
    </xsl:template>
    <xsl:template match="tei:term">
        <i>
            <a>
                <xsl:attribute name="href">
                    <xsl:value-of select="./@target"/>
                </xsl:attribute>
                <xsl:apply-templates/>
            </a>
        </i>
    </xsl:template>
    <xsl:template match="tei:gi">
        <code>
            <xsl:text>&lt;</xsl:text>
            <xsl:value-of select="."/>
            <xsl:text>&gt;</xsl:text>
        </code>
    </xsl:template>
    <xsl:template match="tei:table">
        <br/>
        <table>
            <xsl:for-each select="tei:row">
                <tr style="border: 1px solid #000; margin: 10px 0;">
                    <xsl:if test="position() != 1"/>
                    <xsl:for-each select="tei:cell">
                        <td>
                            <xsl:value-of select="."/>
                        </td>
                        <!-- add a vertical line-->
                        <xsl:if test="position() != last()">
                            <td style="border-left: 1px solid #000;"/>
                        </xsl:if>
                        <xsl:if test="tei:cell[@role = 'label']">
                            <b>
                                <xsl:apply-templates/>
                            </b>
                        </xsl:if>
                    </xsl:for-each>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>

    <!-- DEBUG SECTION - Templates used to show warnings if unexpected elements or attributes -->
    <xsl:template match="tei:*">
        <span class="btn-warning btn-sm" title="Élément non pris en charge par la XSLT.">
            <xsl:text>Élément "</xsl:text>
            <xsl:value-of select="name(.)"/>
            <xsl:text>" non pris en charge par la transformation XSL.</xsl:text>
        </span>
    </xsl:template>
    <!-- END DEBUG SECTION -->
</xsl:stylesheet>

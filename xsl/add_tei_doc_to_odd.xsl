<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:html="http://www.w3.org/1999/xhtml"
    xmlns:tei="http://www.tei-c.org/ns/1.0"   
    exclude-result-prefixes="xs tei html math"
    version="2.0">
    <xsl:output method="xml" omit-xml-declaration="no" indent="yes"/>
    <xsl:strip-space elements="*"/>
    

    <!-- @FM 20/09/2023 Ajoute la documentation TEI dans les balise desc dont le type est "todo" -->
    
    <xsl:template match="* | comment() | processing-instruction()">
        <!-- Copy the element -->
        <xsl:copy>
            <!-- Copy its attributes -->
            <xsl:for-each select="attribute::*">
                <xsl:attribute name="{name(.)}" select="."/>
            </xsl:for-each>
            <!-- Go on in the XML tree -->
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>
    
    
    <!-- Ajoute les descriptions pour les éléments : OK  -->
    <xsl:template match="//tei:elementSpec/tei:desc[@type='todo']">
        <xsl:element name="desc" namespace="http://www.tei-c.org/ns/1.0">
        <!-- met un attribut tei_doc dans le nouveau desc -->
        <xsl:attribute name="type"><xsl:text>tei_doc</xsl:text></xsl:attribute>
            <!-- Mettre le lien vers un dossier avec la documentation de la TEI, trouvable ici : https://sourceforge.net/projects/tei/ -->    
        <xsl:variable name="html_name" select="concat('tei-4.8.0/doc/tei-p5-doc/fr/html/ref-',../@xml:id,'.html')"/>
        <xsl:variable name="documentation_tei" select="document($html_name)"/>
        <xsl:value-of select="substring-after(substring-before($documentation_tei//html:div[@class='main-content']/html:div/html:table/html:tr[1]/html:td[1], '['), '&gt;')"/>
        </xsl:element>
    </xsl:template>
    
    <!-- Ajoute les descriptions pour les attributs : pas très pratique, ça met plusieurs description car ils n'ont pas le meme role en fonction du contexte / probablement inutile  -->
    <!--<xsl:template match="//tei:attDef/tei:desc[@type='todo']">
        <xsl:variable name="ident"><xsl:value-of select="../@ident"/></xsl:variable>
        <xsl:variable name="documentation_tei" select="collection('tei-4.8.0/doc/tei-p5-doc/fr/html/?select=ref-att*')"/>

            <xsl:for-each select="$documentation_tei">
                <!-\- Certains attributs n'ont pas la même description selon le contexte, du coup il faut ensuite choisir le desc qu'on sélectionne.-\->
                <xsl:for-each select="//html:div/html:table/html:tr/html:td/html:table/html:tr/html:td[1]">
                    <xsl:if test="text() = $ident ">
                    <xsl:element name="desc" namespace="http://www.tei-c.org/ns/1.0">
                    <xsl:attribute name="type"><xsl:text>tei_doc</xsl:text></xsl:attribute>
                        <xsl:value-of select="following-sibling::html:td/text()"/> 
                    
                    </xsl:element>
                </xsl:if>
                </xsl:for-each>
            </xsl:for-each>
            </xsl:template>
    -->
</xsl:stylesheet>
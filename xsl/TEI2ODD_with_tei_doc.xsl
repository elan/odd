<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT for generating ODD from one or several XML-TEI file
* version 2023-03-27
* @author AnneGF@CNRS
* @date: 2023
* modified by SerenaCC@UGA
* last edit : 22/08/2024
* modified by FannyMezard@UGA
* last edit : 12/2024
* Ajout de la gestion des modules

* Usage: Put one or more .xml files (XML-TEI) in a subfolder source/ and run a transformation on one (whatever) xml file using this XSL.
* Output: a partial ODD file containing elementSpec, and attList based on what is encoded in the XML-TEI source files
**/
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:html="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="#all" version="2.0">
    
    <xsl:param name="tei_file_dir">/home/mezardf/Nextcloud/ELAN/Projets/Crenum/</xsl:param>
    <xsl:variable name="facsimile_path" select="concat($tei_file_dir, 'XML_TEI/Facsimile/?select=*.xml;recurse=yes')"/>
    <xsl:param name="doc_tei_dir">/home/mezardf/Dev/odd/tei-4.8.1/doc/tei-p5-doc/</xsl:param>
    <xsl:param name="doc_tei_lang">fr</xsl:param>
    

    <xsl:output encoding="UTF-8" method="xml" indent="yes"/>
    <!-- Chemin vers le dossier où sont stockés les fichiers TEI -->
    <xsl:variable name="xml"
        select="collection($facsimile_path)"/>
    <!-- Chemin vers la documentation TEI (TODO : mettre le lien vers Gitlab) -->
    <xsl:variable name="documentation_TEI"
        ><xsl:value-of select="concat($doc_tei_dir, $doc_tei_lang, '/html/ref-')"/></xsl:variable>



    <xsl:template match="/">
        <xsl:result-document href="crenum_facsimile.odd" method="xml">
            <TEI xmlns="http://www.tei-c.org/ns/1.0">
                <teiHeader>
                    <fileDesc>
                        <titleStmt>
                            <title>Manuel d'encodage pour la transcription des fac-similes du projet
                                CRENUM</title>
                            <respStmt>
                                <resp>Création et implémentation de l’odd</resp>
                                <name>Fanny Mézard</name>
                            </respStmt>
                        </titleStmt>
                        <publicationStmt>
                            <authority>UMR 5316 Litt&amp;Arts</authority>
                            <distributor sameAs="#elan">ELAN</distributor>
                            <date when="{format-date(current-date(), '[Y]-[M01]')}">
                                <xsl:value-of
                                    select="format-date(current-date(), '[MNn] [Y]', 'fr', (), ())"
                                />
                            </date>
                            <availability>
                                <licence
                                    target="https://spdx.org/licenses/etalab-2.0.html#licenseText"
                                    >Le schéma d'encodage est mis à disposition selon les termes de
                                    la licence Etalab Open License 2.0 (etalab-2.0)</licence>
                                <p>Ce schéma d'encodage est transformé en rng et html à l'aide d'un
                                    fichier xslt. Les trois formats de sortie du schéma sont
                                    proposés selon les termes de la licence Etalab-2.0.</p>
                            </availability>
                        </publicationStmt>
                        <sourceDesc>
                            <p>Généré à partir d'un fichier .xslt créé par ELAN</p>
                        </sourceDesc>
                    </fileDesc>
                    <profileDesc>
                        <langUsage>
                            <language ident="fr">français</language>
                        </langUsage>
                    </profileDesc>
                    <revisionDesc>
                        <change/>
                    </revisionDesc>
                </teiHeader>
                <text>
                    <body>
                        <schemaSpec ident="NOM_PROJET" start="TEI" prefix="tei_" targetLang="fr"
                            docLang="fr">
                            <!-- Ces 4 modules sont obligatoires, il faut par contre supprimer ces lignes s'ils apparaissent déjà dans le fichier généré 
                            En cas d'erreur, vérifier qu'il ne manque pas un module : les modules des attributs ne sont pas automatiquement ajoutés. -->
                            <moduleRef key="header" include="teiHeader fileDesc titleStmt publicationStmt sourceDesc"/>
                            <moduleRef key="core" include="title p"/>
                            <moduleRef key="tei"/>
                            <moduleRef key="textstructure" include="TEI text body"/>
                            <moduleRef key="linking"/>
                            <xsl:text>
                                
                            </xsl:text>
                            


                            <xsl:for-each-group select="$xml//*"
                                group-by="substring-before(document(concat($documentation_TEI, name(), '.html'))//html:div[@class = 'main-content']/html:div/html:table/html:tr[2]/html:td[2], ' — ')">

                                <xsl:variable name="elem" select="name()"/>

                                <xsl:variable name="html_name"
                                    select="concat($documentation_TEI, $elem, '.html')"/>
                                <xsl:variable name="extrait_tei"
                                    select="document(concat($documentation_TEI, name(), '.html'))"/>
                                <xsl:variable name="module">
                                    <xsl:value-of
                                        select="substring-before($extrait_tei//html:div[@class = 'main-content']/html:div/html:table/html:tr[2]/html:td[2], ' — ')"
                                    />
                                </xsl:variable>
                                <xsl:element name="moduleRef">
                                    <xsl:attribute name="key">
                                        <xsl:value-of select="$module"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="include">
                                        <xsl:for-each-group select="current-group()"
                                            group-by="name()">

                                            <xsl:value-of select="name()"/>
                                            <xsl:message><xsl:value-of select=".[position()]"/></xsl:message>
                                           <xsl:if test="position() &lt; last()"><xsl:text> </xsl:text></xsl:if>
                                        </xsl:for-each-group>

                                    </xsl:attribute>
                                </xsl:element>
                            </xsl:for-each-group>


                            <xsl:for-each-group select="$xml//*" group-by="name()">
                                <xsl:message>===<xsl:value-of select="name()"/>===</xsl:message>
                                <xsl:variable name="elem" select="name()"/>

                                <xsl:variable name="html_name"
                                    select="concat($documentation_TEI, $elem, '.html')"/>
                                <xsl:variable name="extrait_tei" select="document($html_name)"/>
                                <xsl:variable name="module">
                                    <xsl:value-of
                                        select="substring-before($extrait_tei//html:div[@class = 'main-content']/html:div/html:table/html:tr[2]/html:td[2], ' — ')"
                                    />
                                </xsl:variable>
                                <xsl:choose>
                                    <!--or ./@xml:id-->
                                    <xsl:when test=".[not(@*)] or ./@xml:id">
                                        <elementRef key="{name()}"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:sort data-type="text" lang="en"/>
                                        <xsl:choose>
                                            <xsl:when
                                                test="not(contains('instant default full part org status sample', name()))">
                                                <elementSpec ident="{$elem}" mode="change"
                                                  xml:id="{$elem}">
                                                  <attList>

                                                  <xsl:for-each-group select="current-group()/@*"
                                                  group-by="name()">
                                                  <xsl:variable name="att" select="name()"/>
                                                  <attDef ident="{name()}" mode="change">
                                                      <!-- and count($xml//*[name() = $elem]/@*[name() = $att][not(starts-with(., '#'))]) > 0" -->
                                                   
                                                   <!-- Possibilité d'ignorer les xml:id ou d'autres attributs, à régler en fonction des schémas -->
                                                 <!-- <xsl:if test="not(contains('xml:id', $att))">-->
                                                  <valList type="closed" mode="replace">
                                                  <xsl:for-each-group
                                                  select="$xml//*[name() = $elem]/@*[name() = $att]/tokenize(., ' ')"
                                                  group-by=".">
                                                  <xsl:sort data-type="text" lang="fr"/>                                                 
                                                  <valItem ident="{.}"/>
                                                  </xsl:for-each-group>
                                                  </valList>

                                                  <!--</xsl:if>-->
                                                  </attDef>
                                                  </xsl:for-each-group>
                                                  </attList>
                                                </elementSpec>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <elementRef key="{$elem}"/>
                                            </xsl:otherwise>
                                        </xsl:choose>

                                    </xsl:otherwise>
                                </xsl:choose>


                            </xsl:for-each-group>

                        </schemaSpec>
                    </body>
                </text>
            </TEI>
        </xsl:result-document>
    </xsl:template>
</xsl:stylesheet>
